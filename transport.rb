require 'rubocop'
# Transport
class Transport
  def initialize(options)
    @gas = options[:gas]
    @km = options[:km]
    @speed = options[:speed]
  end

  attr_accessor :gas, :km, :speed

  def ride
    if @gas <= 0
      puts 'Gas in the empty'
    else
      @gas -= 0.5
      @km += 12
    end
    tank_condition
    speed_status
  end

  def fill_with_petrol
    @gas = 30
    @speed = 0
    puts 'The auto was petrol'
    tank_condition
    speed_status
  end

  def stop
    exit
  end

  private

  def tank_condition
    if gas < 5
      puts 'Please refill tank'
    elsif gas <= 0
      puts 'The tank is empty'
    end
  end

  def speed_status
    @speed = rand(60) + 50
  end
end
