require "rspec"
require_relative '../transport.rb'

RSpec.describe Transport do
  describe '#transport' do
    let(:transport) { Transport.new(gas: 0.5, km: 0, speed: 60) }
    it { expect(transport.stop).to receive(:exit!) }
  end

  describe '#ride' do
    it { expect(transport.ride(gas: 0)).to eql("Gas in the empty") }
  end

  before :each do
    @transport = Transport.new(gas: 25, km: 0, speed: 60)
  end
end