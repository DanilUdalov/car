require "rspec"
require_relative '../transport.rb'
require_relative '../car.rb'

RSpec.describe Car do
  describe '#car' do
    let(:car) { Car.new(gas: 25, km: 0, speed: 60, model_car: 'Peugeot 407') }

    it { expect(car.stop).to receive(:exit!) }
    it { expect(car.fill_with_petrol).to eql[gas: 30] }
    it { expect(car.ride).to eql[km: 12, gas: 24.5] }
    it { expect(car.open_door_car).to eql[door: true] }
    it { expect(car.close_door_car).to eql[door: false] }
  end
end
