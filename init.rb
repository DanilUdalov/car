require 'rubocop'
require_relative 'transport'
require_relative 'car'

car = Car.new(gas: 10, km: 0, speed: 60, model_car: 'Peugeot 407', door: false)

puts "1. Ride #{car.model_car};"
puts '2. Fill with petrol;'
puts '3. Stop'
puts '4. Open door;'
puts '5. Close door'

bool = true
while bool
  puts "gas = #{car.gas}\nkm = #{car.km}\nspeed = #{car.speed}"
  case gets.chomp
  when '1' then car.ride
  when '2' then car.fill_with_petrol
  when '3' then car.stop
  when '4' then car.open_door_car
  when '5' then car.close_door_car
  end
end
