require 'rubocop'
# Car
class Car < Transport
  def initialize(options)
    @model_car = options[:model_car]
    @door = options[:door]
    super
  end

  attr_accessor :model_car

  def open_door_car
    @door = true
    @speed = 0
    'Door is open'
  end

  def close_door_car
    @door = false
    'Door is close'
  end
end
